import React, { Component } from 'react';
import './App.css';
import Menu from "./components/menu/menu";
import Cart from "./components/cart/cart";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Menu/>
        <Cart/>
      </div>
    );
  }
}

export default App;
