import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://hw69restaurant.firebaseio.com/'
});



export default instance;