import axios from '../../axios-orders';
export const ADD_DISHES = 'ADD_DISHES';
export const REMOVE_DISHES = 'REMOVE_DISHES';
export const GET_DISHES = 'GET_DISHES';

export const getDishesSuccess = (dishes) => ({type: GET_DISHES, dishes});

export const getMenu = () => {
    return (dispatch) => {
        return axios.get('/menu.json').then(response => {
            dispatch(getDishesSuccess(response.data));
        })
    }
};

