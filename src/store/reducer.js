import {GET_DISHES} from "./actions/action";

const initialState = {
    menu: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_DISHES:
            return {...state, menu: action.dishes};

        default: return state
    }
};

export default reducer