import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMenu} from "../../store/actions/action";

import './menu.css';

class Menu extends Component {


    componentDidMount() {
        this.props.getMenu().then(() => {
            console.log(this.props.menu)
        });

    }


    render() {
        return (
            <div style={{margin: '5px'}}>
                <h1>Menu</h1>
                {Object.keys(this.props.menu).map((dish) => {
                    return (
                        <div className="Dish">
                            <img src={this.props.menu[dish].image}
                                 style={{width: '300px', height: '200px', borderRadius: '10px'}}
                                 alt=""/>
                            <p>{dish}</p>
                            <p>{this.props.menu[dish].price} <span>KGS</span></p>
                            <button>Add to cart</button>
                        </div>
                    )
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        menu: state.menu
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMenu: () => dispatch(getMenu())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);